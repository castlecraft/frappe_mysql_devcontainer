#!/bin/bash

echo "Checkout unstaged changes from apps/frappe"
cd apps/frappe
git checkout .
cd ../..

echo "Patching apps/frappe/frappe/database/mariadb/framework_mariadb.sql"
sed -i -e "s/\`name\` varchar(100) DEFAULT NULL,/\`name\` varchar(100),/g" apps/frappe/frappe/database/mariadb/framework_mariadb.sql;

echo "Patching apps/frappe/frappe/database/mariadb/framework_mariadb.sql"
sed -i -e "s/ROW_FORMAT=COMPRESSED/ROW_FORMAT=DYNAMIC/g" apps/frappe/frappe/database/mariadb/framework_mariadb.sql;

echo "Patching apps/frappe/frappe/database/mariadb/database.py"
sed -i -e "s/ROW_FORMAT=COMPRESSED/ROW_FORMAT=DYNAMIC/g" apps/frappe/frappe/database/mariadb/database.py;

echo "Patching apps/frappe/frappe/database/mariadb/schema.py"
sed -i -e "s/ROW_FORMAT=COMPRESSED/ROW_FORMAT=DYNAMIC/g" apps/frappe/frappe/database/mariadb/schema.py;
